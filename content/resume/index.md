---
title: Resume
sitemapExclude: true
---

Here you can find my resume.

* Find the PDF [here](/assets/resume/Hannes_Koerber_Resume.pdf)
* Find the HTML version [here](/assets/resume/index.html)
